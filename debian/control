Source: gfan
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Tim Abbott <tabbott@mit.edu>,
           Cédric Boutillier <boutil@debian.org>,
           Doug Torrance <dtorrance@debian.org>
Homepage: https://users-math.au.dk/jensen/software/gfan/gfan.html
Vcs-Browser: https://salsa.debian.org/math-team/gfan
Vcs-Git: https://salsa.debian.org/math-team/gfan.git
Build-Depends: debhelper-compat (= 13),
               ghostscript,
               html2text,
               libabsl-dev,
               libcdd-dev,
               libgmp-dev,
               texlive-latex-base,
               texlive-latex-recommended,
               texlive-plain-generic
Standards-Version: 4.7.0
Rules-Requires-Root: no

Package: gfan
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: program for computing with Groebner fans
 Gfan is a software package for computing Groebner fans and tropical
 varieties. These are polyhedral fans associated to polynomial
 ideals. The maximal cones of a Groebner fan are in bijection with
 the marked reduced Groebner bases of its defining ideal. The
 software computes all marked reduced Groebner bases of an ideal.
 Their union is a universal Groebner basis. The tropical variety of a
 polynomial ideal is a certain subcomplex of the Groebner fan. Gfan
 contains algorithms for computing this complex for general ideals and
 specialized algorithms for tropical curves, tropical hypersurfaces
 and tropical varieties of prime ideals. In addition to the above core
 functions the package contains many tools which are useful in the
 study of Groebner bases, initial ideals and tropical geometry. Among
 these are an interactive traversal program for Groebner fans and
 programs for graphical renderings.
 .
 For ordinary Groebner basis computations Gfan is not competitive in
 speed compared to programs such as CoCoA, Singular and Macaulay2.
