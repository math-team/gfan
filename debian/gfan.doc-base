Document: gfan
Title: Gfan version 0.6: A User’s Manual
Author: Anders Nedergaard Jensen
Abstract: Gfan is a software package for computing Gröbner fans and tropical
 varieties. These are polyhedral fans associated to polynomial ideals. The
 maximal cones of a Gröbner fan are in bijection with the marked reduced
 Gröbner bases of its defining ideal. The software computes all marked
 reduced Gröbner bases of an ideal. Their union is a universal Gröbner
 basis. The tropical variety of a polynomial ideal is a certain subcomplex
 of the Gröbner fan. Gfan contains algorithms for computing this complex
 for general ideals and specialized algorithms for tropical curves, tropical
 hypersurfaces and tropical varieties of prime ideals. In addition to the
 above core functions the package contains many tools which are useful in
 the study of Gröbner bases, initial ideals and tropical geometry. The full
 list of commands can be found in Appendix B. For ordinary Gröbner basis
 computations Gfan is not competitive in speed compared to programs such
 as CoCoA, Singular and Macaulay2.
Section: Science/Mathematics

Format: PDF
Files: /usr/share/doc/gfan/manual.pdf.gz
