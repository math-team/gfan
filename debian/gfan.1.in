.TH GFAN "1" "@DATE@" "gfan @VERSION@" "Mathematics"
.SH NAME
gfan \- tool to compute Gr\[:o]bner fans and tropical varieties
.SH SYNOPSIS
.B gfan_\fItool\fR [\fIoptions\fR]

.B gfan _\fItool\fR [\fIoptions\fR]
.SH DESCRIPTION
Gfan is a software package for computing Gr\[:o]bner fans and tropical
varieties.  These are polyhedral fans associated to polynomial
ideals. The maximal cones of a Gr\[:o]bner fan are in bijection with
the marked reduced Gr\[:o]bner bases of its defining ideal. The
software computes all marked reduced Gr\[:o]bner bases of an
ideal. Their union is a universal Gr\[:o]bner basis. The tropical
variety of a polynomial ideal is a certain subcomplex of the
Gr\[:o]bner fan. Gfan contains algorithms for computing this complex
for general ideals and specialized algorithms for tropical curves,
tropical hypersurfaces and tropical varieties of prime ideals. In
addition to the above core functions the package contains many tools
which are useful in the study of Gr\[:o]bner bases, initial ideals and
tropical geometry.

The complete list of \fItool\fRs is:

bases, buchberger, combinerays, doesidealcontain, fancommonrefinement,
fanhomology, fanlink, fanproduct, fansubfan, genericlinearchange, groebnercone,
groebnerfan, homogeneityspace, homogenize, initialforms, interactive,
ismarkedgroebnerbasis, krulldimension, latticeideal, leadingterms, list,
markpolynomialset, minkowskisum, minors, mixedvolume, overintegers, padic,
polynomialsetunion, render, renderstaircase, saturation, secondaryfan, stats,
substitute, symmetries, tolatex, topolyhedralfan, tropicalbasis,
tropicalbruteforce, tropicalevaluation, tropicalfunction, tropicalhypersurface,
tropicalintersection, tropicallifting, tropicallinearspace,
tropicalmultiplicity, tropicalrank, tropicalstartingcone, tropicaltraverse,
tropicalweildivisor, version.

Below are presented the generic options available with all \fItool\fRs. For
specific options of a given \fItool\fR, we refer to the help message of that
tool.

.SH OPTIONS
.TP
.B \-\-help
Display a help message describing the functionality of
\fBgfan_\fItool\fR and the available specific options.

.TP
.B \-\-log1, \-\-log2...
Tell Gfan how much information to write to standard error while a computation
is running. These options are very useful when you wish to know if Gfan is
making any progress in its computation.

.TP
.B \-\-stdin \fIvalue\fR
Specify a file to use as input instead of reading from the standard input.

.TP
.B \-\-stdout \fIvalue\fR
Specify a file to write output to instead of writing to the standard output.

.TP
.B \-\-xml
Let polyhedral fans be output in an XML format instead of in the text
format. (The XML files are not readable by Gfan.)

.SH SEE ALSO

The complete manual of Gfan is available from the website of the project:
http://home.imf.au.dk/jensen/software/gfan/gfan.html
